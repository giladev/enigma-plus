import os
import re
import sys  # for sys.version

import pyotherside
import gnupg


def gpg_instance():
    if not os.path.exists('/home/phablet/.local/share/enigma.hummlbach/keys'):
        os.makedirs('/home/phablet/.local/share/enigma.hummlbach/keys')
    gpg = gnupg.GPG(gnupghome='/home/phablet/.local/share/enigma.hummlbach/keys')
    return gpg

def gpg_info():

    gpg = gpg_instance() # gnupg.GPG(verbose = 'advanced', gnupghome='/home/phablet/.local/share/enigma.hummlbach/keys')

    # res = str(gpg.__dict__.keys()) # to print all members of the gpg object

    res =       "GPG binary:   " + gpg.gpgbinary + "\n"
    res = res + "GPG version:  " + str(gpg.version) + "\n"
    res = res + "GPG Encoding: " + gpg.encoding + "\n"
    res = res + "GPG home:     " + str(gpg.gnupghome) + "\n"

    return res

def collect_Python_info():

    res =       "Python version:      " + sys.version + "\n"
    res = res + "PyOtherSide version: " + pyotherside.version + "\n"

    return res


def generate_key(email, password):
    gpg = gpg_instance()
    input_data = gpg.gen_key_input(key_type="RSA",
                                   key_length=4096,
                                   name_email=email,
                                   passphrase=password)
    key = gpg.gen_key(input_data)
    return list_keys()


def list_keys(privateOnly=False):
    gpg = gpg_instance()
    return gpg.list_keys(privateOnly)


def remove_key(fingerprint):
    gpg = gpg_instance()
    gpg.delete_keys(fingerprint, True)
    gpg.delete_keys(fingerprint)
    return list_keys()


def encrypt(filename, key):
    # TODO can we use StandardPath here?
    hub_outgoing = '/home/phablet/.cache/enigma.hummlbach/'+os.path.basename(filename)+'.pgp.txt'
    gpg = gpg_instance()
    try:
        in_stream = open(filename, 'rb')
    except Exception as e:
        pyotherside.send('error', 'Could not open shared file ' + filename + ' for encryption: ' + str(e))
        return None
    else:
        encrypted_data = gpg.encrypt_file(in_stream, [email(key)], output=hub_outgoing)
        in_stream.close()
        if not encrypted_data.ok:
            pyotherside.send('error', 'Encrypting failed with:' + encrypted_data.status)
            return None
        return hub_outgoing

def email(key):
    match = re.search(r'[\w\.-]+@[\w\.-]+', key['uids'][0])
    return match.group(0)


def decrypt(filename, passphrase=None):
    if passphrase == "":
        passphrase = None
    gpg = gpg_instance()
    out_filename = get_name_of_decrypted(filename)
    try:
        in_stream = open(filename, 'rb')
    except Exception as e:
        pyotherside.send('error', 'Could not open shared file ' + filename + ' for decryption: ' + str(e))
        return None
    else:
        decrypted_data = gpg.decrypt_file(in_stream, passphrase=passphrase, output=out_filename)
        in_stream.close()
        if not decrypted_data.ok:
            pyotherside.send('error', 'Decrypting failed with: ' + decrypted_data.status \
                                    + '\nNo PGP message or PGP message corrupted?')
            return None
        return out_filename


def get_name_of_decrypted(filename):
    in_basename = os.path.basename(filename)
    out_basename = ""
    if filename[-4:] == '.pgp':
        out_basename = in_basename[:-4]
    elif filename[-8:] == '.pgp.txt':
        out_basename = in_basename[:-8]
    else:
        root, extension = os.path.splitext(in_basename)
        out_basename = root+'.decrypted'+extension
    return '/home/phablet/.cache/enigma.hummlbach/'+out_basename


def import_key(filename):
    gpg = gpg_instance()
    try:
        in_stream = open(filename, 'rb')
    except Exception as e:
        pyotherside.send('error', 'Could not open keyring ' + filename + ' to import keys: ' + str(e))
        return None
    else:
        key_data = in_stream.read()
        import_result = gpg.import_keys(key_data)
        in_stream.close()
        if import_result.count == 0:
            pyotherside.send('error', 'Imported 0 keys - something must have gone wrong.\
                                       No keyring file or keyring file corrupted?')
            return None
        return list_keys()


def is_encrypted(filename):
    line = ""
    try:
        stream = open(filename, 'rb')
        line = stream.read(30)
    except:
        pass
    finally:
        stream.close()
    if b'BEGIN PGP MESSAGE' in line:
        return True
    return False


def get_key_by_email(emailaddr):
    for key in list_keys():
        if email(key) in emailaddr:
            return key
    return None
#        if email(key) in emailaddr and keyFound:
#            return None
#    return keyFound

