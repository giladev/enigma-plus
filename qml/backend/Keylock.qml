import QtQuick 2.6

// TODO Rename file and ID ("keylock" has no meaning for a programmer, it should be something like "AppViewModel"

// To use the Python element in a QML file, you have to import this QML plugin
import io.thp.pyotherside 1.4  // was 1.3
/*

  PyOtherSide syntax and semantics:

      call(var func, args=[], function callback(result) {})

  Calls the Python function func with args asynchronously. If args is omitted, func will be called without arguments.
  If callback is a callable, it will be called with the Python function result as single argument when the call has succeeded.

*/



// The QML "Python" element of the PyOtherSide plugin exposes a Python interpreter in a QML file
Python {
    id: keylock   // TODO bad name (does not indicate the purpose). How about "appViewModel"? Problem: Mixes PyOtherSide API with APP view model API :-(

    signal encryptionSuccess(string encryptedFile)
    signal decryptionSuccess(string decryptedFile)
    signal importSuccess()
    signal pyError(string errorMessage)   // TODO seems to be unhandled so far (search for "onpyError" in the code)

    property var list: null               // list of public keys
    property var currentKey: null
    property string encryptedFile: ""
    property string decryptedFile: ""
    property string passphrase: ""
    property string gpgInfo: ""        // info from python function gpg_info()
    property string pythonInfo: ""     // mainly Python version and info about PyOtherSide module

    property bool _ongoingEncryption: false
    property bool _ongoingDecryption: false

    function collectGpgInfo() {
        keylock.call('keylock.gpg_info', [], function(result) {
            // aboutPopup.dialogue.column.aboutTextArea.text = result
            keylock.gpgInfo = result;
            console.debug(result);
        })
    }

    // TODO Add property for PyOtherSide info like pluginVersion(), pythonVersion
    function collectPythonInfo    () {
        keylock.call('keylock.collect_Python_info', [], function(result) {

            keylock.pythonInfo = result + 'PyOtherSide plugin version: ' + keylock.pluginVersion() + "\n";
            console.debug(keylock.pythonInfo);
        })
    }

    function generateKey(email, passphrase) {
    /* TODO: the name overloading here is a bit confusing =) */
        keylock.call('keylock.generate_key', [email, passphrase], onKeyGenerated);
    }

    function onKeyGenerated(keylist) {
        currentKey = keylist[keylist.length-1];
        listKeys(keylist);
    }

    function listKeys(keyList) {
        keylock.list = keyList;
    }

    function removeKey(key) {
        keylock.call('keylock.remove_key', [key.fingerprint], listKeys);
    }

    onEncryptedFileChanged: {
        if (encryptedFile != null && encryptedFile != "" && !_ongoingEncryption) {
            _ongoingDecryption = true;
            keylock.call('keylock.decrypt', [encryptedFile, passphrase], onDecrypted);
        }
    }
    
    function onDecrypted(filename) {
        if (filename == null || filename == "") {
            _ongoingDecryption = false;
            return;
        }
        decryptedFile = filename;
        _ongoingDecryption = false;
        decryptionSuccess(filename);
    }

    onDecryptedFileChanged: {

        if (decryptedFile != null && decryptedFile != "" && !_ongoingDecryption) {
            _ongoingEncryption = true;
            keylock.call('keylock.encrypt', [decryptedFile, currentKey], onEncrypted);
        }
    }

    function onEncrypted(filename) {
        if (filename == null || filename == "") {
            _ongoingEncryption = false;
            return;
        }
        encryptedFile = filename;
        _ongoingEncryption = false;
        encryptionSuccess(filename);
    }

    function importKey(filename) {
        keylock.call('keylock.import_key', [filename], onKeyImported);
    }

    function onKeyImported(keylist) {
        if (keylist == null) {
            return;
        }
        currentKey = keylist[keylist.length-1];
        listKeys(keylist);
        importSuccess();
    }

    // This makes the python functions available as "keylock" object
    Component.onCompleted: {
        // The following functions are all from PyOtherSide:
        // https://pyotherside.readthedocs.io/en/latest/
        addImportPath(Qt.resolvedUrl('../../py'));
        importModule('keylock', onModuleImported);
        // setHandler('error', pyError);  // disabled since it looks strange and did swallow error messages
        var mani = Qt.resolvedUrl('../../..') + 'manifest.json'
        console.debug("mani: " + mani)
        // var x = JSON.parse().parse(mani) // TODO Fix SyntaxError: JSON.parse: Parse error
        // readFile(mani)  // Debug output (works)
        // console.debug('manifest.json: '+ x.version)

    }

    // Fetch data at app startup (via QML backend calls) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!11
    function onModuleImported() {
        keylock.call('keylock.list_keys', [], listKeys);
        keylock.collectGpgInfo();
        keylock.collectPythonInfo();
    }

    function isKeyfile(filename) {
        var ext = filename.substr(filename.lastIndexOf('.') + 1);
        return (ext == 'asc');
    }

    function isEncrypted(filename) {
        return keylock.call_sync('keylock.is_encrypted', [filename]);
    }

    function getKeyByEmail(email) {
        return keylock.call_sync('keylock.get_key_by_email', [email]);
    }

    onError: {
        // console.log('python error: ' + traceback);
        console.debug('python error: ' + traceback);
    }

    // If you do not add a special handler on the Python object, such events would be handled by the received() signal handler in QML.
    // Its data parameter contains the event name and all arguments in a list:
    onReceived: {
        // console.log('python error: ' + traceback);
        console.debug('onReceived: ' + data);
    }

    function encrypt(text, key) {
        console.debug("encrypt...")
//        keylock.call('keylock.encrypt', [text, key], function (result) {
//            console.debug("Encrypt result: " + String(result))
//        })
        var result = keylock.call_sync('keylock.encrypt', [text, key])
        console.debug("Encrypt result: " + String(result))
        return (result)
    }
}

