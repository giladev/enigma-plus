import QtQuick 2.0
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import QtQuick.Controls.Suru 2.2
import QtQuick.Controls 2.2

Component {
    id: aboutPopup

// Error: Keylock is not a type
//    // Declare the business model object name
//    Keylock {
//        id: keylock
//    }

    UITK_Popups.Dialog {
        id: dialogue
        title: i18n.tr("About")

        Column {
            id: column
            width: parent.width
            spacing: units.gu(1)

            UITK.TextArea {
                id: aboutTextArea
                height: 400
                readOnly: true
                text: 'Version: ' + Qt.application.version + '\n' + 'App path: ' + Qt.resolvedUrl('../../..') + '\n\n' + keylock.gpgInfo + '\n' + keylock.pythonInfo
                // text: "lorem ipsum\ndixit blah blah blah sadfk öakjlsdfkjladfskölj sdkalfköjladsfkljöadskjlö adfskjlöadsfjklöasdföklakjlösdfköljasdjk löak söldf ökjl asdföjkladösjf"
                //                anchors.bottom: closeButton.top
                //                anchors.bottomMargin: 6
                //                anchors.right: parent.right
                //                anchors.rightMargin: 0
                //                anchors.left: parent.left
                //                anchors.leftMargin: 0
                //                anchors.top: parent.top
                //                anchors.topMargin: 27
                wrapMode: Text.NoWrap
            }

            UITK.Button {
                id: closeButton
                text: i18n.tr("OK")
                anchors.horizontalCenter: parent.horizontalCenter
                width: (parent.width - units.gu(1)) / 2
                //                transformOrigin: Item.Bottom
                //                anchors.horizontalCenter: parent.horizontalCenter
                //                anchors.bottom: parent.bottom
                //                anchors.bottomMargin: 6
                color: Suru.highlightColor
                Suru.highlightType: Suru.PositiveHighlight
                visible: true
                focus: true
                onClicked: {
                    console.debug("aboutPopup.closeButton.onclicked")
                    UITK_Popups.PopupUtils.close(dialogue)
                }
            }
        }

    }

}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
