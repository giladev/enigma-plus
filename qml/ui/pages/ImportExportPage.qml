import QtQuick 2.7
import Ubuntu.Content 1.3
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtQuick.Controls.Suru 2.2
import "../delegates"
import "../components"



Page {
    id: picker
    property var activeTransfer
    property string sharedUrl
    property string receipient: ""
    property string filename: sharedUrl.replace("file://", "")
    property bool isEncryption: false
    property string lastPyError: ""

    header: UITK.PageHeader {
        id: header
        title: i18n.tr('Enigma Plus')

        leadingActionBar.actions: [
            UITK.Action {
                id: cancel
                objectName: "cancel"
                text: i18n.tr("Cancel")
                iconName: "close"
                onTriggered: {
                    pageStack.pop()
                }
            }
        ]

        trailingActionBar.actions: [
            UITK.Action {
                id: okay
                visible: selector.selectedIndex == 2
                objectName: "okay"
                text: i18n.tr("Okay")
                iconName: "tick"
                onTriggered: {
                    keylock.importKey(picker.filename)
                }
            }
        ]
    }

    onFilenameChanged: {
        if (keylock.isKeyfile(filename)) {
            selector.selectedIndex = 2;
        }
        else if (keylock.isEncrypted(filename)) {
            selector.selectedIndex = 0;
        }
        else {
            if (receipient != null && receipient != "" &&
                (key = keylock.getKeyByEmail(receipient)) && key != null) {
                showEncryptToPicker(key);
            }
            else {
                selector.selectedIndex = 1;
            }
        }
    }

    ColumnLayout {
        id: column
        spacing: units.gu(2)
        anchors {
            fill: parent
            topMargin: units.gu(2)
        }

        UITK.OptionSelector {
            id: selector
            anchors {
                left: column.left
                right: column.right
                leftMargin: units.gu(3)
                rightMargin: units.gu(3)
            }
            model: [i18n.tr("Decrypt"),
                    i18n.tr("Select key for encryption"),
                    i18n.tr("Import key")]
        } 

        ScrollView {
            visible: keylock.list != null && selector.selectedIndex == 1 
            width: parent.width
            height: parent.height

            ListView {
                anchors.fill: parent
    
                model: keylock.list
    
                delegate: KeyListItem {
                    key: modelData
                    showRemove: false
                    onClicked: {
                        showEncryptToPicker(key)
                    }
                }
            }
        }

        UITK.TextField {
            id: passphrase
            anchors {
                left: column.left
                right: column.right
                leftMargin: units.gu(3)
                rightMargin: units.gu(3)
            }
            placeholderText: i18n.tr("Passphrase for private key")
            echoMode: TextInput.Password
            visible: selector.model.length != 1 && selector.selectedIndex == 0
        }

        Rectangle {
            id: pickerRectangle
            width: parent.width
            height: parent.height - passphrase.height
            visible: selector.selectedIndex == 0
   
            ContentPeerPicker {
                id: peerPicker
                anchors.fill: parent
                showTitle: false
                contentType: ContentType.All
                handler: ContentHandler.Destination
        
                onPeerSelected: {
                    //peer.selectionType = ContentTransfer.Single
                    console.log("Imported file: " + picker.filename)
                    if (picker.isEncryption) {
                        keylock.decryptedFile = picker.filename;
                    }
                    else {
                        keylock.passphrase = passphrase.text
                        keylock.encryptedFile = picker.filename;
                    }
                }

                function requestTransfer() {
                    picker.activeTransfer = peer.request();
                    picker.activeTransfer.stateChanged.connect(function() {
                        var exportUrl = "";
                        if (picker.isEncryption) {
                            exportUrl = "file://"+keylock.encryptedFile;
                        }
                        else {
                            exportUrl = "file://"+keylock.decryptedFile;
                        }
                        if (picker.activeTransfer.state === ContentTransfer.InProgress) {
                            picker.activeTransfer.items = [
                                resultComponent.createObject(parent, {"url": exportUrl})
                            ];
                            picker.activeTransfer.state = ContentTransfer.Charged;
                            pageStack.pop();
                        }
                        picker.isEncryption = false;
                    });
                }

                onCancelPressed: {
                    pageStack.pop()
                }
            }

            Connections {
                target: keylock

                onEncryptionSuccess: {
                    peerPicker.requestTransfer();
                }

                onDecryptionSuccess: {
                    peerPicker.requestTransfer();
                }

                onImportSuccess: {
                    pageStack.pop();
                }

                onPyError: {
                    picker.lastPyError = errorMessage;
                    UITK_Popups.PopupUtils.open(errorComponent);
                }
            }
        }

        ContentTransferHint {
            id: transferHint
            anchors.fill: parent
            activeTransfer: picker.activeTransfer
        }
    
        Component {
            id: resultComponent
            ContentItem {}
        }
    }

    Component {
        id: errorComponent
        ErrorPopup {
            id: errorPopup

            text: picker.lastPyError

            onClosed: {
                pageStack.pop();
            }
        }
    }

    function showEncryptToPicker(key) {
        picker.isEncryption = true;
        keylock.currentKey = key;
        selector.model = [i18n.tr("Transfer encrypted to...")];
    }
}
