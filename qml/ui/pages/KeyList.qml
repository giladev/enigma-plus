import QtQuick 2.7
import Ubuntu.Components 1.3 as UITK
import Ubuntu.Components.Popups 1.3 as UITK_Popups
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Qt.labs.settings 1.0
import "../delegates"
import "../components"

Page {
    id: keyListPage

    header: UITK.PageHeader {
        id: header
        title: i18n.tr('GPG keys')

        leadingActionBar.actions: [
            UITK.Action {
                id: about
                objectName: "about"
                text: i18n.tr("About")
                iconName: "info"
                onTriggered: {
                    console.debug("onTriggered: about")
                    // aboutPopup.dialogue.aboutTextArea.text = keylock.gpginfo // doubt this works ;-)
                    UITK_Popups.PopupUtils.open(aboutPopup)
                }
            },
            UITK.Action {
                id: test
                objectName: "test"
                text: i18n.tr("Test gpg enc/dec")
                // For a list of available "Suru" icons see:
                // https://docs.ubports.com/projects/icons/en/latest/
                iconName: "sync"
                onTriggered: {
                    // This test requires to create a key manually first for nopassword@test.de with no passphrase!
                    console.debug("onTriggered: test gpg enc/dec")
                    // console.debug(keylock.gpgInfo)
                    var text = "hello world"
                    var fileURL = "file:///home/phablet/.local/share/enigma.hummlbach/contentHubIncoming.txt";
                    console.debug("writeToFile status: " + writeToFile(fileURL, text));
                    var key = keylock.getKeyByEmail("nopassword@test.de")
                    console.debug("Key: " + key["fingerprint"])
                    console.debug("readFile status: " + readFile(fileURL))
//                    keylock.call('keylock.encrypt', [fileURL, key], function(filename) {
                    var fileEnc
                    fileEnc = keylock.encrypt(fileURL.slice(7), key)  // remove the "file://" prefeix to convert the URI to a path
                    console.debug("fileEnc: " + fileEnc)
                    // keylock.call('keylock.decrypt', [encryptedFile, passphrase], onDecrypted);
                    // keylock.gpg
                    console.debug("readFile status (encrypted): " + readFile("file://" + fileEnc))

                    // aboutPopup.dialogue.aboutTextArea.text = keylock.gpginfo // doubt this works ;-)
                }
            }

        ]

        trailingActionBar.actions: [
            UITK.Action {
                id: addKey
                objectName: "addKey"
                text: i18n.tr("Add key")
                iconName: "add"
                onTriggered: {
                    UITK_Popups.PopupUtils.open(addKeyDialog)
                }
            }
        ]
    }

    ScrollView {
        anchors.fill: parent

        ListView {
            id: keyListView
            anchors.fill: parent

            model: keylock.list

            delegate: KeyListItem {
                key: modelData
                onRemoveKey: {
                    UITK_Popups.PopupUtils.open(removeConfirmationComponent)
                }

               Component {
                    id: removeConfirmationComponent
                    ConfirmationPopup {
                        onConfirmed: keylock.removeKey(key)
                        text: i18n.tr("Do you really want to delete the key for %1?").arg(key.uids.toString())
                        confirmButtonText: i18n.tr("Delete")
                        confirmButtonColor: UITK.UbuntuColors.red
                    }
                }
            }
        }
    }

    Item {
		id: welcomeItem

		anchors.top: parent.top
		anchors.topMargin: units.gu(10)
 
		visible: keyListView.count === 0

		width: parent.width
		height: parent.height - header.height

		Column {
			width: parent.width
			height: parent.height
			spacing: units.gu(2)

			anchors.horizontalCenter: parent.horizontalCenter

			UITK.UbuntuShape {
				width: units.gu(34.19); height: units.gu(15)
				anchors.horizontalCenter: parent.horizontalCenter
				radius: "medium"
				image: Image {
				source: Qt.resolvedUrl("../../../assets/logo.svg")
				}
			}

			Label {
				width: parent.width
				horizontalAlignment: Text.AlignHCenter
				font.pointSize: units.gu(2)
				text: i18n.tr("Start by adding PGP keys")
			}

			Label {
				width: parent.width
				horizontalAlignment: Text.AlignHCenter
				text: i18n.tr("Click the '+' at the top to generate a new key or\n import your keyring by sharing an asc file to Enigma.")
			}
		}
    }

    AddKeyDialog {
        id: addKeyDialog
    }

    AboutPopup {
        id: aboutPopup
    }

}
