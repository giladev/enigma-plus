# Enigma Plus

A PGP app for UBuntu Touch forked from the **Enigma** app (aka Keylock) originally implemented and published by Johannes Renkl <hummlbach@posteo.de> at https://gitlab.com/hummlbach/keylock

The original **Enigma** app (published at [open-store.io](https://open-store.io/app/enigma.hummlbach) did only support the `armhf` architecture but newer phones
like the [Volla Phone](https://volla.online/de/) or [PinePhone](https://www.pine64.org/pinephone/) do use the `arm64` architecture (aka `aarch64`).

So the near-term goals of this fork are:

- support `arm64` architecture too
- improve the UI (usability)
- show more status info and logs to the user
- improve error handling
- add screen shots of all UI pages here
- publish a new version at [open-store.io](https://open-store.io/)

## How to build

1. Install the build system `clickable` as described here:

   https://clickable.bhdouglass.com/en/latest/install.html

   I have used the ppa version on my Ubuntu 20.04 client computer.

2. Enable the developer mode on your Phone (requires to set a passphrase + do a complete restart (power down cycle):

   https://docs.ubports.com/en/latest/userguide/advanceduse/adb.html

   Important: `adb devices` must find your phone (if connected to your computer via USB)!

3. Clone `Enigma Plus` via Git from this repo into a folder of your computer

   `git clone https://gitlab.com/giladev/enigma-plus.git`

4. `cd` into the root folder of the source code

   `cd enigma-plus`

5. Build, run and test it on your desktop computer (`amd64`):

    ```
    clickable --config clickable-desktop.json desktop
    ```

6. Build and install Enimga Plus on your phone (while the phone is connected to your computer via USB):

   For `arm64`:
   ```
   clickable --arch arm64
   ```

   For `armhf`:
   ```
   clickable --arch armhf
   ```

Now the Enimga app should be installed on your phone :-)



## Developing in Qt Creator:

```
# within the project root folder:
clickable ide qtcreator
# Then open the `CMakeLists.txt` file to get a project view
# Just build & run it on your desktop from within Qt Creator
```


## Implementation details

The ``Enigma Plus`` implementation uses

- Qt QML for the user interface ([see the Ubuntu Touch API docs](https://api-docs.ubports.com/sdk/apps/qml/index.html)
- Python for the backend implementation (using the Qt plugin [PyOtherSide](https://github.com/thp/pyotherside) for async Python code calls from the QML UI
- the Ubuntu Touch "clipboard"-alike [content hub](https://docs.ubports.com/en/latest/appdev/guides/importing-CH-urldispatcher.html) infrastructure on the phone
  to interact with other apps to receive and send files for encryption and decryption
- the [gnupg](https://pythonhosted.org/gnupg/gnupg.html#gnupg-module) python module for an API to interact with the `gpg` binaries via command line
- the most recent `gpg` command line tools (packaged and published by the target architecture maintainers for Ubuntu Touch and Ubuntu Desktop).
  See [scripts/get_libs.py](https://gitlab.com/giladev/enigma-plus/-/blob/master/scripts/get_libs.py) on how this works
- CMake for builds (dependencies)
- [clickable](https://clickable-ut.dev) to build (compile), package, deploy/install and publish at open-store.io



## How the UI is bound to the backend logic at runtime?

The call and data flow is difficult to understand due to the asnc calls of the python code and the QML bindings and signals.

Here is high-level view (reverse-engineered):

1. `qmlscene qml/ui/Main.qml`: Start of the apolication (see the `Enigma.desktop.in` file.

   [qmlscene](https://doc.qt.io/qt-5/qtquick-qmlscene.html) is a ultility that loads and displays QML documents.
   `qmlscene` is meant for developing only normally so it is unclear why this tool is used)

2. `Main.qml`: The main UI of the app. Creates an App window with a `page stack` where sub windows can be placed "on top" of each other

3. `Main.qml` loads the `backend\Keylock.qml` file via import and adds it as element with the name `keylock`:
   This is the view model for QML to access the core non-GUI logic by calling Python via the `PyOtherSide` plugin
   - Provides QML functions for the "business logic" (via async calls to Python functions from QML)
   - provide data as properties (data model)
   - provides signals to indicate business logic events to the UI

4. `Main.qml` connects to the `ContentHub` ("clipboard") of the phone by subscribing "share requests"

5. `Main.qml` adds the `pages/KeyList.qml` UI to the page stack as default UI (if no share request was received but the App opened directly by the user)
   to displays a list of vailable `gpg` keys.

6. `KeyList.qml` TODO

